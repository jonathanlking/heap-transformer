{-# LANGUAGE DataKinds, FlexibleContexts, OverloadedLists #-}

module Main where

import           Control.Monad.Heap.Pure

import           Control.Monad.Identity
import           Control.Monad.Reader
import           Data.IntMap             (IntMap)
import qualified Data.IntMap             as M
import           Data.IntSet             (IntSet)
import qualified Data.IntSet             as S
import           Data.Maybe
import           Data.Monoid             (Endo (..))
import           Data.Vector             (Vector)
import           Paths_heap_transformer
import           System.Random
import           Text.Parsec
import qualified Text.Parsec.Language    as Lang
import qualified Text.Parsec.Token       as Tok

type Point = Int
type PtMap = IntMap
type PtSet = IntSet

type PherLvl = Double

data Ant s = Ant { current    :: !(Ref s Point)
                 , notVisited :: !(Ref s PtSet)
                 , pathLength :: !(Ref s Double)
                 , path       :: !(Ref s [Point])
                 }

data Link s = Link { pherLvl :: !(Ref s PherLvl)
                   , dist    :: !Double
                   }

instance Show (Link s) where
  show Link { dist = dist } = show ("Link " ++ show dist)

type City s = PtMap (Link s)

type Graph s = PtMap (City s)

type T = [ PtSet
         , Point
         , StdGen
         , Double
         , [Point]
         ]

data R s = R { graph      :: !(Graph s)
             , stdGenRef  :: !(Ref s StdGen)
             , pherInfl   :: Double
             , distInfl   :: Double
             , evapCoeff  :: Double
             , pherPerAnt :: Double
             }

type AntM s = ReaderT (R s) (HeapT s T Identity)

newAnt :: Point -> AntM s (Ant s)
newAnt city = do
  R { graph = graph } <- ask
  lift $ do
    current <- newHeapRef city
    notVisited <- newHeapRef (S.delete city (M.keysSet graph))
    pathLength <- newHeapRef 0
    path <- newHeapRef [city]
    pure Ant { current = current
             , notVisited = notVisited
             , pathLength = pathLength
             , path = path
             }

rand :: Random a => (a, a) -> AntM s a
rand range = do
  R { stdGenRef = stdGenRef } <- ask
  stdGen <- lift $ readHeapRef stdGenRef
  let (res, nextStdGen) = randomR range stdGen
  lift $ writeHeapRef stdGenRef nextStdGen
  pure res

getCityMap :: Int -> AntM s (City s)
getCityMap pt = do
  R { graph = graph } <- ask
  case M.lookup pt graph of
    Nothing   -> error ("City number: " ++ show pt ++ " is not in the map.")
    Just city -> pure city

type Done = Bool

step :: Ant s -> AntM s Done
step Ant { current = currentRef
         , notVisited = notVisited
         , pathLength = pathLength
         , path = path
         } = do
  current <- lift $ readHeapRef currentRef
  R { graph = graph
    , pherInfl = pi
    , distInfl = di
    } <- ask
  let links = fromJust $ M.lookup current graph
  nvs <- lift $ readHeapRef notVisited
  (totalProb, probMap) <-
    M.foldrWithKey (\city Link { dist = dist
                               , pherLvl = pherRef
                               } acc -> do
                       (totalProb, list) <- acc
                       if (city `S.member` nvs)
                       then do
                         pher <- lift $ readHeapRef pherRef
                         let prob = pher ** pi / dist ** di
                         pure (totalProb + prob, (city, (dist, prob)) : list)
                       else pure (totalProb, list)) (pure (0, [])) links
  case probMap of
    [] -> pure True
    ((fstCity, (fstLen, _)) : follow) -> do
      picker <- rand (0.0, totalProb)
      let foldProb picker cpt routes =
            case routes of
              (city, (len, prob)) : routes -> if picker > 0
                                              then foldProb (picker - prob) (len, city) routes
                                              else (len, city)
              _ -> cpt
          (len, city) = foldProb picker (fstLen, fstCity) follow
      lift $ do
        writeHeapRef currentRef city
        modifyHeapRef' notVisited (S.delete city)
        modifyHeapRef' pathLength (+ len)
        modifyHeapRef' path (city :)
        pure False

propagate :: Ant s -> AntM s ()
propagate Ant { pathLength = pathLengthRef
              , path = pathRef
              } = do
  pathLen <- lift $ readHeapRef pathLengthRef
  path <- lift $ readHeapRef pathRef
  R { graph = graph
    , evapCoeff = evapCoeff
    , pherPerAnt = ppa
    } <- ask
  let updatePath (c1 : cs@(c2 : _)) = do
        let Link { pherLvl = c1Toc2Ref } = fromJust $ do
              lks <- M.lookup c1 graph
              M.lookup c2 lks
        -- The reference c1 -> c2 and c2 -> c1 are shared
        currPherLvl <- lift $ readHeapRef c1Toc2Ref
        let pherLvl = (1 - evapCoeff) * currPherLvl + ppa / pathLen
        lift $ writeHeapRef c1Toc2Ref pherLvl
        updatePath cs
      updatePath _ = pure ()
  updatePath path

runAnt :: Ant s -> AntM s Double
runAnt ant = do
  runUntilDone ant
  let Ant { pathLength = pathLengthRef } = ant
  lift $ readHeapRef pathLengthRef
  where runUntilDone ant = do
          done <- step ant
          unless done $ runUntilDone ant
          propagate ant

newGraph :: Monad m => IntMap (Double, Double) -> HeapT s T m (Graph s)
newGraph graph = ($ M.empty) <$> mkLinks
  where cities = M.toList graph
        createLinks (c1, (x1, y1)) (c2, (x2, y2)) = do
          ref <- newHeapRef 0
          let xd = x1 - x2
              yd = y1 - y2
              dist = fromIntegral $ round $ sqrt (xd * xd + yd * yd)
              link = Link { pherLvl = ref
                          , dist = dist
                          }
          pure (Endo (M.insertWith M.union c1 (M.singleton c2 link) .
                      M.insertWith M.union c2 (M.singleton c1 link)))

        mkLinks = fmap (appEndo . mconcat) $
                  sequence $
                  [ createLinks (c1, pos1) (c2, pos2)
                  | (c1, pos1) <- cities
                  , (c2, pos2) <- cities
                  , c1 < c2
                  ]

estimateSolution :: StdGen
                 -> IntMap (Double, Double)
                 -> Int -- Ants to start
                 -> Point -- Starting point
                 -> Int -- Number of runs
                 -> (Double, StdGen) -- Length
estimateSolution stdGen graph antNum start iter = runIdentity $ runHeapT heapT
  where es len 0 = do
          R { stdGenRef = stdGenRef } <- ask
          stdGen <- lift $ readHeapRef stdGenRef
          pure (len, stdGen)
        es len n = do
          ants <- replicateM antNum (newAnt start)
          let runPar [] = pure []
              runPar (ant : ants) = do
                len <- runAnt ant
                lens <- runPar ants
                pure (len : lens)
          nlen <- minimum <$> runPar ants
          es (min len nlen) (n - 1)

        heapT = do
          graph <- newGraph graph

          stdGenRef <- newHeapRef stdGen

          runReaderT (es (1/0) iter) R { graph = graph
                                       , stdGenRef = stdGenRef
                                       , pherInfl = 10
                                       , distInfl = 2
                                       , evapCoeff = 0.05
                                       , pherPerAnt = 10000
                                       }

int :: Parsec String () Int
double :: Parsec String () Double
(int, double) = (fromIntegral <$> Tok.integer tokParser, double)
  where tokParser = Tok.makeTokenParser Lang.haskellDef
        double = do
          res <- Tok.naturalOrFloat tokParser
          case res of
            Left int     -> pure $ fromIntegral int
            Right double -> pure double

parseLine :: Parsec String () (Int, (Double, Double))
parseLine = do
  _ <- many space
  idx <- int
  _ <- many space
  x <- double
  y <- double
  pure (idx, (x, y))

parser :: Parsec String () (IntMap (Double, Double))
parser = do
  _ <- manyTill anyChar (try (string "NODE_COORD_SECTION"))
  lines <- manyTill parseLine (try (string "eof" <|> string "EOF"))
  pure $ M.fromList lines

main :: IO ()
main = do
  stdGen <- getStdGen
  filePath <- getDataFileName "bench/data/a280.tsp"
  file <- readFile filePath
  case parse parser "(unknown)" file of
    Left err -> error ("Parse failed: " ++ show err)
    Right graph -> do
      let (sol, _) = estimateSolution stdGen graph 10 1 5
      print sol
