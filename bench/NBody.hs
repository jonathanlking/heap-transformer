{-# LANGUAGE DataKinds, FlexibleContexts, OverloadedLists #-}

module Main where

import           Control.Monad.Heap.Pure
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Criterion.Main
import           Data.Vector             (Vector)
import qualified Data.Vector             as V
import           System.Environment

data V3 a = V3 !a !a !a

solarMass :: Double
solarMass = 4 * pi * pi

daysPerYear :: Double
daysPerYear = 365.24

data Body c = Body { pos  :: !(V3 c)
                   , vel  :: !(V3 c)
                   , mass :: !Double
                   }

type MBody s = Body (Ref s Double)

jupiter :: Body Double
jupiter = Body { pos = V3 4.84143144246472090e+00
                          (-1.16032004402742839e+00)
                          (-1.03622044471123109e-01)
               , vel = V3 (1.66007664274403694e-03 * daysPerYear)
                          (7.69901118419740425e-03 * daysPerYear)
                          (-6.90460016972063023e-05 * daysPerYear)
               , mass = 9.54791938424326609e-04 * solarMass
               }

saturn :: Body Double
saturn = Body { pos = V3 8.34336671824457987e+00
                         4.12479856412430479e+00
                         (-4.03523417114321381e-01)
               , vel = V3 (-2.76742510726862411e-03 * daysPerYear)
                          (4.99852801234917238e-03 * daysPerYear)
                          (2.30417297573763929e-05 * daysPerYear)
               , mass = 2.85885980666130812e-04 * solarMass
               }

uranus :: Body Double
uranus = Body { pos = V3 1.28943695621391310e+01
                         (-1.51111514016986312e+01)
                         (-2.23307578892655734e-01)
               , vel = V3 (2.96460137564761618e-03 * daysPerYear)
                          (2.37847173959480950e-03 * daysPerYear)
                          (-2.96589568540237556e-05 * daysPerYear)
               , mass = 4.36624404335156298e-05 * solarMass
               }

neptune :: Body Double
neptune = Body { pos = V3 1.53796971148509165e+01
                          (-2.59193146099879641e+01)
                          1.79258772950371181e-01
               , vel = V3 (2.68067772490389322e-03 * daysPerYear)
                          (1.62824170038242295e-03 * daysPerYear)
                          (-9.51592254519715870e-05 * daysPerYear)
               , mass = 5.15138902046611451e-05 * solarMass
               }

sun :: Body Double
sun = Body { pos = V3 0 0 0
           , vel = V3 0 0 0
           , mass = solarMass
           }

system :: Vector (Body Double)
system = [ sun
         , jupiter
         , saturn
         , uranus
         , neptune
         ]

offsetMomentum :: (Monad m, HasElem Double ts)
               => MBody s
               -> V3 Double
               -> HeapT s ts m ()
offsetMomentum Body { vel = V3 vx vy vz } (V3 px py pz) = do
  writeHeapRef vx (-px / solarMass)
  writeHeapRef vy (-py / solarMass)
  writeHeapRef vz (-pz / solarMass)

initBody :: (Monad m, HasElem c ts)
         => Body c
         -> HeapT s ts m (Body (Ref s c))
initBody Body { pos = V3 x y z
              , vel = V3 vx vy vz
              , mass = m
              } = do
  x <- newHeapRef x
  y <- newHeapRef y
  z <- newHeapRef z

  vx <- newHeapRef vx
  vy <- newHeapRef vy
  vz <- newHeapRef vz

  pure Body { pos = V3 x y z
            , vel = V3 vx vy vz
            , mass = m
            }

type T = '[Double]

type NBodyM s = ReaderT (Vector (MBody s)) (HeapT s T Identity)

advance :: Double -> NBodyM s ()
advance dt = do
  bodies <- ask
  let mapBodies i iBody = do
        let remBodies = V.drop (i + 1) bodies
            mapRemBodies j jBody = do
              let V3 ix iy iz = pos iBody
              ix <- readHeapRef ix
              iy <- readHeapRef iy
              iz <- readHeapRef iz
              let V3 jx jy jz = pos jBody
              jx <- readHeapRef jx
              jy <- readHeapRef jy
              jz <- readHeapRef jz
              let dx = ix - jx
                  dy = iy - jy
                  dz = iz - jz

                  dSquared = dx * dx + dy * dy + dz * dz
                  distance = sqrt dSquared
                  mag = dt / (dSquared * distance)

                  V3 ivxr ivyr ivzr = vel iBody

              modifyHeapRef' ivxr (subtract (dx * mass jBody * mag))
              modifyHeapRef' ivyr (subtract (dy * mass jBody * mag))
              modifyHeapRef' ivzr (subtract (dz * mass jBody * mag))
        V.imapM_ mapRemBodies remBodies
  lift $ do
    V.imapM_ mapBodies bodies
    V.mapM_ (\body -> do
                let V3 vxr vyr vzr = vel body
                    V3 xr yr zr = pos body
                vx <- readHeapRef vxr
                vy <- readHeapRef vyr
                vz <- readHeapRef vzr
                modifyHeapRef' xr (+ (dt * vx))
                modifyHeapRef' yr (+ (dt * vy))
                modifyHeapRef' zr (+ (dt * vz))) bodies

energy :: NBodyM s Double
energy = do
  bodies <- ask
  lift $ do
    er <- newHeapRef 0
    let mapBodies i iBody = do
          let V3 vxr vyr vzr = vel iBody
          vx <- readHeapRef vxr
          vy <- readHeapRef vyr
          vz <- readHeapRef vzr
          let dSquared = vx * vx + vy * vy + vz * vz
          modifyHeapRef' er (+ (0.5 * mass iBody * dSquared))
          let remBodies = V.drop (i + 1) bodies
              mapRemBodies j jBody = do
                let V3 ixr iyr izr = pos iBody
                    V3 jxr jyr jzr = pos jBody
                ix <- readHeapRef ixr
                iy <- readHeapRef iyr
                iz <- readHeapRef izr
                jx <- readHeapRef jxr
                jy <- readHeapRef jyr
                jz <- readHeapRef jzr

                let dx = ix - jx
                    dy = iy - jy
                    dz = iz - jz

                    distance = sqrt (dx * dx + dy * dy + dz * dz)

                modifyHeapRef' er (subtract ((mass iBody * mass jBody) / distance))
          V.imapM_ mapRemBodies remBodies

    V.imapM_ mapBodies bodies
    readHeapRef er

runSystem :: Int -> (Double, Double)
runSystem n = runIdentity $ runHeapT $ do
  system <- traverse initBody system
  runReaderT (energy >>= \initial -> runSystem' initial n) system
  where runSystem' :: Double -> Int -> NBodyM s (Double, Double)
        runSystem' initial n
          | n <= 0 = do
              final <- energy
              pure (initial, final)
          | otherwise = do
              advance 0.01
              runSystem' initial (n - 1)

main :: IO ()
main = do
  let (init, end) = runSystem 50000000
  print init
  print end
