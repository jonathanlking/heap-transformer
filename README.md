# heap-transformer

This library provides transformer for the `ST` monad called `HeapT`
that is safe to use with any monad, unlike
[`STT`](https://hackage.haskell.org/package/STMonadTrans-0.4.3). It is
not as flexible as `STT`, as it is necessary to specify doesn't allow
to hold references on all types, but only on types that are
explicitly.

# Example

The [`bench`](bench/) directory contains two examples that make use of
`HeapT`. Note that, despite the name of the directory they are in,
they are not really proper benchmarks, but they will be in a near
future.
