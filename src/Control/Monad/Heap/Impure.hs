{-# LANGUAGE BangPatterns, ConstraintKinds, DataKinds, FlexibleContexts, FlexibleInstances, GADTs,
             GeneralizedNewtypeDeriving, KindSignatures, MultiParamTypeClasses, OverloadedLists,
             PolyKinds, RankNTypes, ScopedTypeVariables, TupleSections, TypeFamilies,
             TypeOperators, TypeSynonymInstances, UndecidableInstances #-}

module Control.Monad.Heap.Impure ( Ref
                                 , HeapT
                                 , runHeapT
                                 , newHeapRef
                                 , readHeapRef
                                 , writeHeapRef
                                 , modifyHeapRef
                                 , modifyHeapRef'
                                 , HasElem
                                 , InitHeaps
                                 , Erased
                                 , erase
                                 , runErased
                                 ) where

import           Data.Elem

import           Control.Monad.Reader
import           Control.Monad.State
import           Control.Monad.Writer
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Maybe
import           System.IO.Unsafe
import           System.Mem.Weak

-- | The type of references.  It carries the type of the value it points to as well as a phantom type to prevent it from leaking outside the computation it was created in. Internally, it is a boxed wrapper around an 'Int'.
data Ref s a = Ref Int

instance Eq (Ref s a) where
  Ref n == Ref m = n == m

instance Ord (Ref s a) where
  Ref n `compare` Ref m = n `compare` m

-- | A 'Heap' containing values of type @a@. It contains a dictionnary indexed by 'Ref' pointing to 'Weak a', an integer source, giving the value of the next free reference in the heap and a cleanup counter. The cleanup counter is decremented after each operation and the heap is cleaned of dereferenced pointers each time it reaches @0@.
data Heap s a = Heap { heap      :: !(Map (Ref s a) (Weak a))
                     , refSource :: !Int
                     , cleanup   :: !Int
                     }

-- | A list containing 'Heap's pointing to different types of value. The type level list is the list of the different types the @Heap@s contain.
data Heaps :: * -> [*] -> * where
  Bottom :: Heaps s '[]
  Push :: Heap s a -> Heaps s as -> Heaps s (a ': as)

-- | The 'HeapT' monad transformer. The type-level list @ts@ is the list of type it is possible to have references to.
newtype HeapT s ts m a = HeapT (StateT (Heaps s ts) m a)
                       deriving( Functor
                               , Applicative
                               , Monad
                               , MonadTrans
                               )

instance MonadReader r m => MonadReader r (HeapT s ts m) where
  ask = lift ask
  local f (HeapT a) = HeapT (local f a)

instance MonadWriter r m => MonadWriter r (HeapT s ts m) where
  writer = lift . writer
  listen (HeapT a) = HeapT (listen a)
  pass (HeapT a) = HeapT (pass a)

instance MonadState r m => MonadState r (HeapT s ts m) where
  get = lift get
  put = lift . put

-- | We will cleanup a heap each time we have done 'cleanupEvery' operations on it. This number is hardcoded for now.
cleanupEvery :: Int
cleanupEvery = maxBound

-- | Cleanup the heap when the @cleanup@ counter reaches @0@.
cleanupHeap :: Heap s a -> Heap s a
cleanupHeap hp@Heap { heap = heap
                    , cleanup = cnp
                    }
  | cnp <= 0 =
    hp { heap = M.filter (isJust . unsafePerformIO . deRefWeak) heap
       , cleanup = cleanupEvery
       }
  | otherwise = hp { cleanup = cnp - 1 }

-- A helper function to implement newHeapRef, readHeapRef, etc.
modifyHeap :: Elem a as
           -> (Heap s a -> (b, Heap s a))
           -> Heaps s as
           -> (b, Heaps s as)
modifyHeap Here fun (Push heap heaps) = (res, Push (cleanupHeap newHeap) heaps)
  where (res, newHeap) = fun heap
modifyHeap (There later) fun (Push heap heaps) = (res, Push heap newHeaps)
  where (res, newHeaps) = modifyHeap later fun heaps

newHeapRef :: (Monad m, HasElem a as) => a -> HeapT s as m (Ref s a)
newHeapRef val =
  HeapT $ do
  heaps <- get
  let modHeap hp@Heap { heap = heap
                      , refSource = idx
                      } =
        -- We purposefully don't use ref as the key in the map, so
        -- that it can be garbage collected
        (ref, hp { heap = M.insert (Ref idx) ptr heap
                 , refSource = idx + 1
                 })
        where ref = Ref idx
              ptr = unsafePerformIO (mkWeak ref val Nothing)
      (ref, newHeaps) = modifyHeap elemProof modHeap heaps
  put newHeaps
  pure ref

readHeapRef :: (Monad m, HasElem a as) => Ref s a -> HeapT s as m a
readHeapRef ref =
  HeapT $ do
  heaps <- get
  let modHeap hp@Heap { heap = heap } =
        case M.lookup ref heap of
          Nothing  -> error "Ooopsie" -- Some meaningful error messages
          Just ptr ->
            case unsafePerformIO (deRefWeak ptr) of
              Nothing  -> error "Oh no! The GC collected our references but we still had it :("
              Just val -> (val, hp)
      !(res, _) = modifyHeap elemProof modHeap heaps
  pure res

writeHeapRef :: (Monad m, HasElem a as) => Ref s a -> a -> HeapT s as m ()
writeHeapRef ref val =
  HeapT $ do
  heaps <- get
  let modHeap ref hp@Heap { heap = heap } =
        ((), hp { heap = M.insert ref ptr heap })
        where ptr = unsafePerformIO (mkWeak ref val Nothing)
      ((), newHeaps) = modifyHeap elemProof (modHeap ref) heaps
  put newHeaps

modifyHeapRef :: (Monad m, HasElem a as) => Ref s a -> (a -> a) -> HeapT s as m ()
modifyHeapRef ref fun = do
  val <- readHeapRef ref
  writeHeapRef ref $ fun val

modifyHeapRef' :: (Monad m, HasElem a as) => Ref s a -> (a -> a) -> HeapT s as m ()
modifyHeapRef' ref fun = do
  val <- readHeapRef ref
  writeHeapRef ref $! fun val

---------------------------------------------------------------------------
-- Running HeapT
---------------------------------------------------------------------------

-- Will generate a list of empty heaps for the given list of types
class InitHeaps (ts :: [*]) where
  mkHeaps :: Heaps s ts

instance InitHeaps '[] where
  mkHeaps = Bottom

instance InitHeaps ts => InitHeaps (t : ts) where
  mkHeaps = Push Heap { heap = []
                      , refSource = 0
                      , cleanup = cleanupEvery
                      } mkHeaps

runHeapT :: (Monad m, InitHeaps ts)
         => (forall s. HeapT s ts m a)
         -> m a
runHeapT (HeapT act) = evalStateT act mkHeaps

-- There's a bit of magic left. Consider we're writing a typechecker
-- for the simply typed lambda calculus with destructive unification
-- (where the type variables carry a reference to the eventual other
-- type they've been unified with). Here's the the type of types:
--
-- data Type s = TVar { varIndex :: Int
--                    , varInst  :: Ref s (Maybe (Type s))
--                    }
--             | TFun (Type s) (Type s)
--
-- The type checker will be an action of a type like this:
--
-- HeapT s [Type s] m a
--
-- But one can't call runHeapT on a value of this type, because ts =
-- [Type s] depends on s. However it shouldn't matter, because the
-- only thing that shouldn't depend on s is a. The solution is then to
-- erase the list of types from HeapT as follow:

data Erased s m a where
  Erase :: InitHeaps ts => HeapT s ts m a -> Erased s m a

erase :: InitHeaps ts => HeapT s ts m a -> Erased s m a
erase = Erase

runErased :: Monad m
          => (forall s. Erased s m a)
          -> m a
runErased (Erase (HeapT heapT)) = evalStateT heapT mkHeaps
